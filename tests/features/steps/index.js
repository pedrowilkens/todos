import Steps  from '../../lib/Steps'

const setupAndTeardown = {

    async beforeAll() {
        await page.setViewport({width: 1300, height: 1000})
    },

    async afterAll() {
    },

    async beforeEach() {

    },

    async afterEach() {
    }

}


Steps.add(/^I click on button$/, async () => {

})

Steps.add(/^Im on the page$/, async () => {
    await page.goto('http://localhost:3000')
})


export { Steps, setupAndTeardown }
