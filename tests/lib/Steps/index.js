class Steps {
    constructor() {
        this.list = []
    }

    add(regExp, func) {
        const isAlreadyAdded = this.list.find((step) => String(step.regExp) === String(regExp))
        if (isAlreadyAdded) {
            throw Error(String(regExp) + ' : Step is already added. Please remove duplicate.')
        }

        this.list.push({regExp, func})
    }

    /**
     * merge steps from different files into one Step
     * @param lists
     */
    concat(...lists) {
        this.list = this.list.concat(...lists)
    }

    /**
     * returns a function which matches a corresponding .feature string
     */
    get(title) {
        let params
        const found = this.list.find((step) => {
            const matches = title.match(step.regExp)

            if (matches) {
                params = matches.slice(1)
                return true
            }

            return false
        })

        if (!found) {
            throw Error(title + ' : was not defined in Steps file')
        }

        return found.func.bind(this, ...params)
    }
}

export default new Steps()
