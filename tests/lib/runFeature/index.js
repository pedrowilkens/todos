import { defineFeature, loadFeature } from 'jest-cucumber'
// All Steps and a General setupAndTeardown
import { Steps, setupAndTeardown } from '../../features/steps'

jest.setTimeout(200 * 1000)


/**
 * @param beforeAndAfter:   If it is given this, a custom setupAndTearDown
 *                          will be used, other wise the general steps setupAndTearDown will be used
 *
 */
module.exports.run = function run(featureFile, beforeAndAfter) {

    let tagFilter = 'not @skip'
    if(process.env.TEST_FILTER_TAGS && process.env.TEST_FILTER_TAGS.length) {
        tagFilter = process.env.TEST_FILTER_TAGS
    }

    let feature
    if (tagFilter === '@all') {
        tagFilter = ''
        feature = loadFeature(featureFile)
    } else {
        feature = loadFeature(featureFile, { tagFilter })
    }

    if (!beforeAndAfter) {
        beforeAndAfter = setupAndTeardown
    }

    // const hasBackground = !_.isEmpty(feature.background)

    defineFeature(feature, (test) => {
        // before Feature
        beforeAll(beforeAndAfter.beforeAll)
        // after feature
        afterAll(beforeAndAfter.afterAll)
        // before each scenario
        beforeEach(beforeAndAfter.beforeEach)
        // after each scenario
        afterEach(beforeAndAfter.afterEach)

        feature.scenarios.forEach((scenario) => {

            test(scenario.title, (stepName) => {
                scenario.steps.forEach((step) => {
                    const currentStep = Steps.get(step.stepText)
                    // Wraps current step with screenshot capability, so that it makes separate screenshots for failed and succeed Steps
                    stepName[step.keyword](step.stepText, currentStep)
                })
            })
        })
    })
}

/*
{
    "title": "Golden Path",
    "scenarios": [{
        "title": "1: Expert signs in",
        "steps": [{
            "stepText": "I am at the page \"signIn-new\"",
            "keyword": "given",
            "stepArgument": null,
            "lineNumber": 5
        }, {
            "stepText": "I enter \"Anton Aachen\" 's username in the login form",
            "keyword": "when",
            "stepArgument": null,
            "lineNumber": 6
        }, {
            "stepText": "I enter \"Anton Aachen\" 's password in the login form",
            "keyword": "and",
            "stepArgument": null,
            "lineNumber": 7
        }, {
            "stepText": "I see the page \"cockpit\"",
            "keyword": "then",
            "stepArgument": null,
            "lineNumber": 8
        }],
        "tags": ["@included"],
        "lineNumber": 4
    }],
        "tags": ["@included"],
        "lineNumber": 11
    }],
    "scenarioOutlines": [],
    "tags": [],
    "options": {
        "tagFilter": "@included and not @excluded",
        "errors": {
            "missingScenarioInStepDefinitions": true,
            "missingStepInStepDefinitions": true,
            "missingScenarioInFeature": true,
            "missingStepInFeature": true
        }
    }
}

*/
