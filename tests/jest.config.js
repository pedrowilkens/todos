const jestConfig = {
    'verbose': true,
    'preset': 'jest-puppeteer',
    'testMatch': ['**/tests/suites/index.spec.js'],
    'globalSetup': './global-setup.js',
    'globalTeardown': './global-teardown.js',
    'testEnvironment': './custom-environment.js',
}


module.exports = jestConfig