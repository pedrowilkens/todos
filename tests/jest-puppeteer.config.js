module.exports = {
    launch: {
        headless: process.env.RUN_IN_BROWSER !== 'debug',
        slowMo: 40,
        devtools: true,
        args: [
            '--window-size=1920,1080',
            '--disable-background-timer-throttling',
            '--disable-backgrounding-occluded-windows',
            '--disable-renderer-backgrounding'
        ]
    },
    browserContext: 'default',
}
