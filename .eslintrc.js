module.exports = {
    'env': {
        'browser': true,
        'commonjs': true,
        'commonjs': true,
        'es6': true,
        'node': true
    },
    'extends': 'eslint:recommended',
    'globals': {
        'Atomics': 'readonly',
        'SharedArrayBuffer': 'readonly',
        'page': 'readonly',
        'describe': "readonly",
        'it': 'readonly',
        'jest': 'readonly',
        'beforeEach': 'readonly',
        'beforeAll': 'readonly',
        'afterAll': 'readonly',
        'afterEach': 'readonly',
        'Template': 'readonly',
        'ReactiveVar': 'readonly',
        '_': 'readonly',
    },
    'parserOptions': {
        'ecmaVersion': 2018,
        "sourceType": "module"
    },
    'rules': {
        'indent': [
            'error',
            4
        ],
        'linebreak-style': [
            'error',
            'unix'
        ],
        'quotes': [
            'error',
            'single'
        ],
        'semi': [
            'error',
            'never'
        ]
    }
}
